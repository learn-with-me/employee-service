package com.example.employee.util;

import com.example.employee.model.Employee;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.Arrays;
import java.util.List;

public class TestHelper {

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static List<Employee> employeeList() {
        return Arrays.asList(
                new Employee("1", "firstname", "lastname"),
                new Employee("2", "firstname", "lastname"),
                new Employee("3", "firstname", "lastname"),
                new Employee("4", "firstname", "lastname")
        );
    }

    public static List<Employee> getEmployeesFromResponse(String contentAsString) {
        Gson gson = new Gson();
        return gson.fromJson(contentAsString, new TypeToken<List<Employee>>(){}.getType());
    }
}
