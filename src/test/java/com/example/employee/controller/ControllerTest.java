package com.example.employee.controller;

import com.example.employee.model.Employee;
import com.google.gson.Gson;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static com.example.employee.util.TestHelper.asJsonString;
import static com.example.employee.util.TestHelper.getEmployeesFromResponse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void shouldThrowExceptionForTheGivenEmployeePayloadWithEmptyNameField() throws Exception {

        mvc.perform( MockMvcRequestBuilders
                .put("/1.0/employee")
                .content(asJsonString(new Employee("102", "", "lastname")))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldReturnNotFound() throws Exception {

        mvc.perform( MockMvcRequestBuilders
                        .get("/1.0/employee/{id}", 100)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                        .andDo(print())
                        .andExpect(status().isNotFound());
    }

    @Test
    public void shouldReturnAnEmployee() throws Exception {

        mvc.perform( MockMvcRequestBuilders
                        .get("/1.0/employee/{employeeNumber}", "001003")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getAllEmployeesAPI() throws Exception {

        mvc.perform( MockMvcRequestBuilders
                        .get("/1.0/employee")
                        .accept(MediaType.APPLICATION_JSON))
                        .andDo(print())
                        .andExpect(status().isOk());
    }

    @Test
    public void getAllEmployeesByFirstNameAndLastNameAPI() throws Exception {

        final MvcResult result = mvc.perform(MockMvcRequestBuilders
                        .get("/1.0/employee/firstName/{firstName}/lastName/{lastName}", "David", "Hepburn")
                        .accept(MediaType.APPLICATION_JSON))
                        .andDo(print())
                        .andExpect(status().isOk())
                        .andReturn();

        final String contentAsString = result.getResponse().getContentAsString();
        assertEquals(getEmployeesFromResponse(contentAsString).get(0).getFirstName(), "David");
    }

    @Test
    public void getAllEmployeesByFirstNameAndLastNameApi_InASCOrder() throws Exception {

        final MvcResult result = mvc.perform(MockMvcRequestBuilders
                        .get("/1.0/employee/firstName/{firstName}/lastName/{lastName}", "David", "Hepburn")
                        .accept(MediaType.APPLICATION_JSON)
                        .param("order","ASC"))
                        .andDo(print())
                        .andExpect(status().isOk())
                        .andReturn();

        final String contentAsString = result.getResponse().getContentAsString();
        assertEquals(getEmployeesFromResponse(contentAsString).get(0).getFirstName(), "David");
    }

    @Test
    public void getAllEmployeesByFirstNameAndLastNameApi_InDESCOrder() throws Exception {

        final MvcResult result = mvc.perform(MockMvcRequestBuilders
                        .get("/1.0/employee/firstName/{firstName}/lastName/{lastName}", "David", "Hepburn")
                        .accept(MediaType.APPLICATION_JSON)
                        .param("order","DESC"))
                        .andDo(print())
                        .andExpect(status().isOk())
                        .andReturn();

        final String contentAsString = result.getResponse().getContentAsString();
        assertEquals(getEmployeesFromResponse(contentAsString).get(0).getFirstName(), "Audrey");
    }
}
