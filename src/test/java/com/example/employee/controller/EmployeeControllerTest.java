package com.example.employee.controller;

import com.example.employee.model.Employee;
import com.example.employee.service.EmployeeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static com.example.employee.util.TestHelper.employeeList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class EmployeeControllerTest {

    @InjectMocks
    private EmployeeController employeeController;

    @Mock
    private EmployeeService service;

    @Test
    public void shouldGetEmployeeForTheGivenEmployeeId() {
        String employeeNumber = "001003";
        Employee employee = new Employee(employeeNumber, "firstname", "lastname");
        when(service.getEmployee(employeeNumber)).thenReturn(employee);
        ResponseEntity<Employee> employeeResponseEntity = this.employeeController.getEmployee(employeeNumber);
        assertEquals(HttpStatus.OK, employeeResponseEntity.getStatusCode());
        assertEquals(employee, employeeResponseEntity.getBody());
    }

    @Test
    public void shouldGetAllEmployees() {
        final List<Employee> employees = employeeList();
        when(service.findAllEmployees()).thenReturn(employees);
        ResponseEntity<List<Employee>> employeeResponseEntity = this.employeeController.getEmployees();
        assertEquals(HttpStatus.OK, employeeResponseEntity.getStatusCode());
        assertEquals(employees, employeeResponseEntity.getBody());
        assertEquals(employees.size(), Objects.requireNonNull(employeeResponseEntity.getBody()).size());
    }

    @Test
    public void shouldGetNoEmployees() {
        final List<Employee> employees = Collections.emptyList();
        when(service.findAllEmployees()).thenReturn(employees);
        ResponseEntity<List<Employee>> employeeResponseEntity = this.employeeController.getEmployees();
        assertEquals(HttpStatus.NOT_FOUND, employeeResponseEntity.getStatusCode());
    }

    @Test
    public void shouldGetEmployeeNotFoundException() {
        String employeeNumber = "001001";
        when(service.getEmployee(employeeNumber)).thenReturn(null);
        ResponseEntity<Employee> employeeResponseEntity = this.employeeController.getEmployee(employeeNumber);
        assertEquals(HttpStatus.NOT_FOUND, employeeResponseEntity.getStatusCode());
    }

    @Test
    public void shouldUpdateEmployeeForTheGivenEmployeePayload() {
        String employeeNumber = "001003";
        Employee employee = new Employee(employeeNumber, "new_firstname", "lastname");
        when(service.updateEmployee(employee)).thenReturn(employee);
        ResponseEntity<Employee> employeeResponseEntity = this.employeeController.updateEmployee(employee);
        assertEquals(HttpStatus.OK, employeeResponseEntity.getStatusCode());
        assertEquals(employee, employeeResponseEntity.getBody());
        assertEquals(employee.getFirstName(), Objects.requireNonNull(employeeResponseEntity.getBody()).getFirstName());
    }

}
