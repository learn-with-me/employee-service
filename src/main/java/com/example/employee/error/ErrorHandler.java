package com.example.employee.error;

import com.example.employee.model.ErrorCode;
import com.example.employee.model.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.example.employee.model.ErrorCode.EMPLOYEE_NOT_FOUND;
import static com.example.employee.model.ErrorCode.INVALID_REQUEST;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Slf4j
@RestControllerAdvice
public class ErrorHandler extends ResponseEntityExceptionHandler {

    private static final String FIELD_ERROR_MESSAGE = " Field failed with message --> ";

    private Function<FieldError,String> fetchErrorMessage =
            error -> error.getField() + FIELD_ERROR_MESSAGE + error.getDefaultMessage();

    @ExceptionHandler({EmployeeNotFoundException.class})
    @ResponseStatus(NOT_FOUND)
    @ResponseBody
    public ErrorResponse handleEmployeeNotFoundException(EmployeeNotFoundException exception) {
        return logErrorAndRespondInvalidRequest(EMPLOYEE_NOT_FOUND, Collections.singletonList(exception.getMessage()));
    }

    @Override
    @ResponseStatus(BAD_REQUEST)
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {

        final List<String> strings =
                ex.getFieldErrors().stream()
                        .map(fetchErrorMessage)
                        .collect(Collectors.toList());
        return ResponseEntity.badRequest().body(logErrorAndRespondInvalidRequest(INVALID_REQUEST, strings));
    }

    private ErrorResponse logErrorAndRespondInvalidRequest(ErrorCode errorCode, List<String> errors) {
        ErrorResponse errorResponse = new ErrorResponse(UUID.randomUUID().toString(),
                errorCode, errors,
                LocalDateTime.now());
        String logMessage = "Request failed at %s with following error %s at %s";
        log.error(String.format(logMessage, LocalDateTime.now(), errorResponse.getErrorMessage(),
                errorResponse.getErrorOccurredAt()));
        return errorResponse;
    }
}
