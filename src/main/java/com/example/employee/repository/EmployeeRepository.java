package com.example.employee.repository;

import com.example.employee.repository.entity.EmployeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<EmployeeEntity, Long> {

    Optional<EmployeeEntity> findByEmployeeNumber(String employeeNumber);

    @Modifying
    @Query("UPDATE EmployeeEntity e SET e.firstname = :firstName, e.lastname = :lastName WHERE e.employeeNumber = :employeeNo")
    int update(@Param("firstName") String firstName, @Param("lastName") String lastName, @Param("employeeNo") String employeeNo);

    List<EmployeeEntity> findByFirstnameOrLastname(String fname, String lname);

}
