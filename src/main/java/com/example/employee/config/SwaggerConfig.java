package com.example.employee.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;

@Configuration
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("CAPCO Employee API")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.example.employee"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "My Employees REST API",
                "Various Endpoint exposed to GET an Employee, GET all Employee, Update an Employee ets",
                "v1.0",
                "Terms of service",
                new Contact("Dhiren Kumar", "https://www.linkedin.com/in/dhirenkp/", "dhirenkpatra@gmail.com"),
                "License of API", "API license URL", Collections.emptyList());
    }

}