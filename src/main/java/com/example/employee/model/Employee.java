package com.example.employee.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Value;

import javax.validation.constraints.NotBlank;

@Value
@ApiModel(description="All details about the Employee.. ")
public class Employee {

    String userId;

    @ApiModelProperty(
            value = "First name of the Employee and Cannot Be Empty",
            name = "firstName",
            dataType = "String",
            example = "Dhiren")
    @NotBlank(message = "First Name Cannot Be Empty")
    String firstName;

    @ApiModelProperty(
            value = "Last name of the Employee and Cannot Be Empty",
            name = "firstName",
            dataType = "String",
            example = "Patra")
    @NotBlank(message = "Last Name Cannot Be Empty")
    String lastName;

}
