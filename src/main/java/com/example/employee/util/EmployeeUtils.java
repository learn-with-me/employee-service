package com.example.employee.util;

import com.example.employee.model.Employee;

import java.util.Comparator;

public class EmployeeUtils {

    public static Comparator<Employee> employeeComparator = (o1,o2) -> {
        int firstNameCompare = o1.getFirstName().compareTo(
                o2.getFirstName());
        int lastNameCompare = o1.getLastName().compareTo(
                o2.getLastName());
        return (firstNameCompare == 0) ? lastNameCompare : firstNameCompare;
    };
}
