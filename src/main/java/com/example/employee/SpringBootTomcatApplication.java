package com.example.employee;

import com.example.employee.config.HSQLDBConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class SpringBootTomcatApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootTomcatApplication.class, args);
	}

	@Bean
	@ConditionalOnProperty(prefix = "hsql", name = "service", havingValue = "enabled")
	public HSQLDBConfiguration configuration() {
		return new HSQLDBConfiguration();
	}

}
