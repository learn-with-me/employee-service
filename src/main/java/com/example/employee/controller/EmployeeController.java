package com.example.employee.controller;

import com.example.employee.model.Employee;
import com.example.employee.service.EmployeeService;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;

import static com.example.employee.util.EmployeeUtils.employeeComparator;

@RestController
@RequestMapping("/1.0/employee")
@Slf4j
@Api("EmployeeController")
public class EmployeeController {

    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping("/{employeeNumber}")
    @ApiOperation(value = "GET Particular Employee", notes = "GET an individual Employee by it's id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success|OK", response = Employee.class),
            @ApiResponse(code = 404, message = "not found!!!") })
    public ResponseEntity<Employee> getEmployee(@PathVariable("employeeNumber") String employeeNumber) {

        Employee employee = employeeService.getEmployee(employeeNumber);
        if (employee != null) {
            return new ResponseEntity<>(employee, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping
    @ApiOperation(value = "GET All Employees", notes = "Fetch All Employees")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success|OK", response = Employee.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "not found!!!") })
    public ResponseEntity<List<Employee>> getEmployees() {
        List<Employee> employee = employeeService.findAllEmployees();
        return employee.isEmpty() ? ResponseEntity.notFound().build() : ResponseEntity.ok(employee);
    }

    @PutMapping
    @ApiOperation(value = "UPDATE Employee", notes = "Update Employee")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success|OK", response = Employee.class),
            @ApiResponse(code = 404, message = "not found!!!") })
    public ResponseEntity<Employee> updateEmployee(@Valid @RequestBody Employee employee) {
        return ResponseEntity.ok(employeeService.updateEmployee(employee));
    }

    @GetMapping("/firstName/{firstName}/lastName/{lastName}")
    @ApiOperation(value = "GET List of Employees", notes = "List Employees using First name and Last name")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success|OK", response = Employee.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "not found!!!") })
    public ResponseEntity<List<Employee>> getEmployee(@PathVariable String firstName,
                                                      @PathVariable String lastName,
                                                      @RequestParam(required=false) String order) {
        List<Employee> employees =
                employeeService.searchEmployeeByFirstnameOrLastname(firstName, lastName);
        if("DESC".equalsIgnoreCase(order)) {
            employees.sort(employeeComparator);
            return new ResponseEntity<>(employees, HttpStatus.OK);
        }
        return new ResponseEntity<>(employees, HttpStatus.OK);
    }
}
