package com.example.employee.service;

import com.example.employee.error.EmployeeNotFoundException;
import com.example.employee.model.Employee;
import com.example.employee.repository.EmployeeRepository;
import com.example.employee.repository.entity.EmployeeEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    @Override
    public Employee getEmployee(String employeeNumber) {
        Optional<EmployeeEntity> employeeEntity = employeeRepository.findByEmployeeNumber(employeeNumber);
        if (employeeEntity.isPresent()) {
            return mapEmployee(employeeEntity.get());
        }
        throw new EmployeeNotFoundException("Employee not found for employeeNumber:" + employeeNumber);
    }

    @Override
    public List<Employee> findAllEmployees() {
        return employeeRepository.findAll().stream().map(this::mapEmployee).collect(Collectors.toList());
    }

    private Optional<EmployeeEntity> findEmployeeById(String id) {
        return employeeRepository.findByEmployeeNumber(id);
    }

    @Override
    @Transactional
    public Employee updateEmployee(Employee employee) {
        return findEmployeeById(employee.getUserId())
                .map((e) -> mapFieldToBeUpdated(employee))
                .orElseThrow(() -> new EmployeeNotFoundException("Employee With ID "+employee.getUserId()+ " Not Found."));
    }

    @Override
    public List<Employee> searchEmployeeByFirstnameOrLastname(String firstName, String lastName) {
        final List<EmployeeEntity> byFirstnameOrLastname =
                employeeRepository.findByFirstnameOrLastname(firstName, lastName);
        if(byFirstnameOrLastname.isEmpty())
            throw new EmployeeNotFoundException
                    ("Employee With First Name "+firstName+ " And Last Name "+lastName+" Not Found.");
        return byFirstnameOrLastname.stream().map(this::mapEmployee).collect(Collectors.toList());
    }

    private Employee mapFieldToBeUpdated(Employee entity) {
        employeeRepository.update(entity.getFirstName(), entity.getLastName(), entity.getUserId());
        return entity;
    }

    private Employee mapEmployee(EmployeeEntity employeeEntity) {
        return new Employee(employeeEntity.getEmployeeNumber(), employeeEntity.getFirstname(), employeeEntity.getLastname());
    }
}
