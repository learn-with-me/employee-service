package com.example.employee.service;

import com.example.employee.model.Employee;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface EmployeeService {

    Employee getEmployee(String employeeNumber);
    List<Employee> findAllEmployees();
    Employee updateEmployee(Employee employee);
    List<Employee> searchEmployeeByFirstnameOrLastname(String firstName, String lastName);

}
