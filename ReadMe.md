# Employees API

This project handles various api's related to employee.

## Enhancements Done TO Exercise

- DOCKER Functionality added
- For Dockerized environment HSQLDB GUI Disabled
- Validation Improvements
- Swagger Documentations
- Postman Collections Added and Shared below in the link

## Installing and Running The App

Basic GIT Commands
```
 git clone https://LearnWithMe@bitbucket.org/learn-with-me/employee-service.git
 
 After cloning move to the project root directory 
 in any IDE/Terminal, then execute below commands 
 as per your system status: 
 
 1) Docker Environment Run
 2) Normal MVN Spring Boot Run
 3) Java -jar run
```

Running By Pulling Docker Image
```
docker pull dhiren13/employee-service:latest
docker run -e "SPRING_PROFILES_ACTIVE=docker" -p 9083 dhiren13/employee-service:latest
```

Running the application by Maven
```
mvn clean install
mvn spring-boot:run
```
Running the application using java -jar command
```
mvn clean install
java -jar .\target\employee-service-0.0.1-SNAPSHOT.jar
```
Running the application using docker (if installed locally)
```
docker build --tag=employee-service:latest .
docker run -e "SPRING_PROFILES_ACTIVE=docker" -p 9083:9083 employee-service:latest
```

Find the SWAGGER DOCS URL --> [HERE](http://localhost:9083/swagger-ui/index.html)

POSTMAN Collections Link --> [HERE](https://www.getpostman.com/collections/09ff66f4dcef95de9656)

## Design Consideration Made
- **Added Validation** around employee object for UPDATE API, like first name and last name can' t be empty.
- It throws BAD_REQUEST if validation failed on the above.
- Controller Testcases added using @MockMvc to check on status codes.
- **Swagger** Documentations added for the exposed api's.
- Error Message field **errorMessage** converted to list to accommodate multiple failure messages.
- **Order by ASC and DESC** feature enabled for api search using first name and last name

## End points Exposed
- *GET ALL* http://localhost:9083/1.0/employee
- *GET ONE* http://localhost:9083/1.0/employee/{emp_id}
- *GET ALL* http://localhost:9083/1.0/employee/firstName/{fname}/lastName/{lname}?order=ASC
- *UPDATE* http://localhost:9083/1.0/employee

#### 1) GET Individual Employee by ID
>- curl -X GET "http://localhost:9083/1.0/employee/12" -H "accept: */*"

#### 2) GET All Employees
>- curl -X GET "http://localhost:9083/1.0/employee" -H "accept: */*"

#### 3) Search Employees Based on their First Name and Last Name even we can order by ASC/DESC.
>- curl -X GET "http://localhost:9083/1.0/employee/firstName/David/lastName/Warner?order=ASC" -H "accept: */*"

#### 4) UPDATE Employee by passing firstname and lastname as non-empty fields.
>- curl -X PUT "http://localhost:9083/1.0/employee" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"firstName\": \"Dhiren\", \"lastName\": \"Patra\", \"userId\": \"12\"}"

## Payload Used for PUT
```json
{
  "userId" : "001000",
  "firstName" : "David",
  "lastName" : "Warner"
}
```

## Response(1) - Single Object
```json
{
  "userId" : "001000",
  "firstName" : "David",
  "lastName" : "Warner"
}
```

## Response(2) - List of Objects
```json
[
  {
    "userId": "001000",
    "firstName": "David",
    "lastName": "Beckham"
  },
  {
    "userId": "001001",
    "firstName": "Audrey",
    "lastName": "Hepburn"
  }
]
```

## Error Response
```json
{
  "id": "729b6d9b-ee30-4c0a-9c8d-95d5f969c7b0",
  "errorCode": "EMPLOYEE_NOT_FOUND",
  "errorMessage": [
    "Employee not found for employeeNumber:00102300"
  ],
  "errorOccurredAt": "2021-10-14T21:10:52.42"
}
```
```json
{
    "id": "001713a9-aae5-45ee-aac7-524de5c5f5b6",
    "errorCode": "INVALID_REQUEST",
    "errorMessage": [
        "lastName Field failed with message --> Last Name Cannot Be Empty",
        "firstName Field failed with message --> First Name Cannot Be Empty"
    ],
    "errorOccurredAt": "2021-10-14T21:11:25.683"
}
```

## Contributing
Pull requests are welcome.

## Connect With Me

Dhiren Kumar Patra - See my Profile at [@linked_in](https://www.linkedin.com/in/dhirenkp/)

Link to My Docker Hub ---- [DOCKER_HUB](https://hub.docker.com/u/dhiren13)

Project Link: [FIND_HERE](https://bitbucket.org/learn-with-me/employee-service/src/master/)
