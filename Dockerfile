FROM openjdk:8u111-jdk-alpine
ARG DEPENDENCY=target/dependency
EXPOSE 9083
COPY ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY ${DEPENDENCY}/META-INF /app/META-INF
COPY ${DEPENDENCY}/BOOT-INF/classes /app

ADD target/employee-service-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-cp","app:app/lib/*","com.example.employee.SpringBootTomcatApplication"]
